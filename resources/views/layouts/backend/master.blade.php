<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <title>Starter Page | Skote - Responsive Bootstrap 4 Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description">
    <meta content="Themesbrand" name="author">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('backend/assets/images/favicon.ico') }}">

    <!-- Bootstrap Css -->
    <link href="{{ asset('backend/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Icons Css -->
    <link href="{{ asset('backend/assets/css/icons.min.css') }}" rel="stylesheet" type="text/css">
    <!-- App Css-->
    <link href="{{ asset('backend/assets/libs/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ asset('backend/assets/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('backend/assets/libs/alertify/css/alertify.min.css') }}">

    <link href="{{ asset('backend/assets/css/app.min.css') }}"  rel="stylesheet" type="text/css">
{{--    <link href="{{ asset('css/style.css') }}"  rel="stylesheet" type="text/css">--}}
    <link rel="stylesheet" href="{{ asset('backend/assets/css/style.css') }}" type="text/css">
    @livewireStyles
</head>

<body data-sidebar="dark">

<!-- Begin page -->
<div id="layout-wrapper">

    <header id="page-topbar">
        @include('layouts.backend.header')
    </header>

    <!-- ========== Left Sidebar Start ========== -->

    <!-- Left Sidebar End -->
@include('layouts.backend.sidebar')
<!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                @yield('content')
            </div> <!-- container-fluid -->
        </div>
        <!-- End Page-content -->


        @include('layouts.backend.footer')
    </div>
    <!-- end main content-->

</div>
<!-- END layout-wrapper -->

<!-- Right bar overlay-->

<!-- JAVASCRIPT -->
<script src="{{ asset('backend/assets/libs/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('backend/assets/libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('backend/assets/libs/metismenu/metisMenu.min.js') }}"></script>
<script src="{{ asset('backend/assets/libs/simplebar/simplebar.min.js') }}"></script>
<script src="{{ asset('backend/assets/libs/node-waves/waves.min.js') }}"></script>
<script src="{{ asset('backend/assets/libs/select2/js/select2.min.js') }}"></script>
<script src="{{ asset('backend/assets/libs/alertify/alertify.min.js') }}"></script>
{{--<script src="{{ asset('backend/asset/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>--}}
{{--<script src="{{ asset('backend/asset/libs/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>--}}
{{--<script src="{{ asset('backend/asset/libs/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>--}}
{{--<script src="{{ asset('backend/asset/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>--}}
{{--<script src="{{ asset('backend/asset/libs/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>--}}
{{--<script src="{{ asset('backend/asset/libs/chenfengyuan/datepicker/datepicker.min.js') }}"></script>--}}

{{--<script src="{{ asset('backend/assets/js/pages/form-advanced.init.js') }}"></script>--}}

<script src="{{ asset('backend/assets/libs/sweetalert2/sweetalert2.min.js') }}"></script>

<!-- Sweet alert init js-->
<script src="{{ asset('backend/assets/js/app.js') }}"></script>
@livewireScripts
<script>
    window.addEventListener('alertify', event => {
        switch (event.detail.type)
        {
            case 'warning':
                alertify.warning(event.detail.message);
                break
            case 'success':
                alertify.success(event.detail.message);
                break
            case 'error':
                alertify.error(event.detail.message);
                break
        }
    })
    window.addEventListener('swal.confirm', event => {
        Swal.fire({
            showCancelButton: true,
            confirmButtonColor: "#3ddc97",
            cancelButtonColor: "#f46a6a",
            title: event.detail.title,
            text: event.detail.text,
            type: event.detail.type,
        }).then(willDelete => {
            if(willDelete.value === true) {
                Livewire.emit(event.detail.action, event.detail.id);
            }
        })
    })
</script>
@yield('scripts')
</body>
</html>
