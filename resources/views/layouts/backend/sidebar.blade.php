<div class="vertical-menu">

    <div data-simplebar="" class="h-100">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>

                <li>
                    <a href="{{ route('backend.dashboard.index') }}" class="waves-effect">
                        <i class="bx bx-home-circle"></i>
                        <span>Dashboards</span>
                    </a>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bxl-facebook"></i>
                        <span>Facebook</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{ route('backend.connect.index') }}">Connection</a></li>
                        <li><a href="{{ route('backend.fan-page.index') }}">Fan Page</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="bx bxs-chip"></i>
                        <span>Log</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{ route('backend.log-error.index') }}">Log Error</a></li>
                        <li><a href="{{ route('backend.log-sync.index') }}">Log Sync</a></li>
                    </ul>
                </li>

                <li>
                    <a href="{{ route('backend.setting.index') }}" class="waves-effect">
                        <i class="bx bx-cog"></i>
                        <span>Setting</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
