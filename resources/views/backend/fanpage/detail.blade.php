@extends('layouts.backend.master')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0 font-size-18">Detail</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Facebook</a></li>
                    <li class="breadcrumb-item active">Detail</li>
                </ol>
            </div>

        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                @livewire('fan-page-form', ['pageVideo' => new \App\Models\Mongo\FacebookPageVideo(), 'facebookPage' => $facebookPage])
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                @livewire('fan-page-video-list', ['facebookPage' => $facebookPage])
            </div>
        </div>
    </div>
</div>
@endsection
