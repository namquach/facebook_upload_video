@extends('layouts.backend.master')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="mb-0 font-size-18">Settings</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('backend.setting.index') }}">Settings</a></li>
                        <li class="breadcrumb-item active">List</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="">
                        @foreach($settings as $setting)
                            @if($setting->meta_key === 'secret_key')
                                {!! renderInput($errors, $setting->meta_key, ['value' => $setting->meta_value, 'type' => 'password']) !!}
                            @else
                                {!! renderInput($errors, $setting->meta_key, ['value' => $setting->meta_value]) !!}
                            @endif
                        @endforeach
                        <button class="btn btn-success">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
