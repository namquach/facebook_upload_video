@extends('layouts.auth.master')

@section('content')
    <div class="card overflow-hidden">
        <div class="bg-soft-primary">
            <div class="row">
                <div class="col-7">
                    <div class="text-primary p-4">
                        <h5 class="text-primary">Welcome Back !</h5>
                        <p>Sign in to continue to Skote.</p>
                    </div>
                </div>
                <div class="col-5 align-self-end">
                    <img src="{{ asset('backend/assets\images\profile-img.png') }}" alt="" class="img-fluid">
                </div>
            </div>
        </div>
        <div class="card-body pt-0">
            <div>
                <a href="#">
                    <div class="avatar-md profile-user-wid mb-4">
                            <span class="avatar-title rounded-circle bg-light">
                                <img src="{{ asset('backend/assets\images\logo.svg') }}" alt="" class="rounded-circle" height="34">
                            </span>
                    </div>
                </a>
            </div>
            <div class="p-2">
                <form class="form-horizontal" method="post" action="{{ route('auth.authenticate') }}">
                    @csrf
                    {!! renderInput($errors, 'email', ['placeholder' => 'Enter email', 'value' => 'qhnam.67@gmail.com']) !!}

                    {!! renderInput($errors, 'password', ['type' => 'password', 'value' => '']) !!}

                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="customControlInline">
                        <label class="custom-control-label" for="customControlInline">Remember me</label>
                    </div>

                    <div class="mt-3">
                        <button class="btn btn-primary btn-block waves-effect waves-light" type="submit">Log In</button>
                    </div>
                    <div class="mt-4 text-center">
                        <a href="#l" class="text-muted"><i class="mdi mdi-lock mr-1"></i> Forgot your password?</a>
                    </div>
                </form>
            </div>

        </div>
    </div>

@endsection
