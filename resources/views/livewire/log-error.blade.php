<div>
    <div wire:loading>
        <x-loading />
    </div>
    <div class="table-responsive">
        <div class="container">
            <div class="row">
                <div class="col-md-12" style="display: flex">

                </div>
            </div>
        </div>
        <br>
        <table class="table table-nowrap table-centered mb-0">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Message</th>
                <th scope="col">Created</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                @foreach($logErrors as $key => $logError)
                    <tr>
                        <td>{{ getNumberRowTable($logErrors, $key) }}</td>
                        <td>{{ substr(@$logError->exception['message'] ?? '', 0, 60) }} ...</td>
                        <td>{{ $logError->created_at }}</td>
                        <td>
                            <a href="{{ route('backend.log-error.show', ['id' => $logError->_id]) }}" class="btn btn-primary btn-sm">View</a>
                            <button wire:click="deleteConfirm('{{ $logError->_id }}')" class="btn btn-danger btn-sm">Delete</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $logErrors->links() }}

    </div>
</div>
