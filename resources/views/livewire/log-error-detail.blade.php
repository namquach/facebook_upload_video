<div class="table-responsive">
   <table class="table table-nowrap table-centered mb-0">
       <tr>
           <th>ID</th>
           <th>{{ $logError->_id }}</th>
       </tr>
       <tr>
           <th>Exception</th>
           <th><pre>{!! json_encode($logError->exception, JSON_PRETTY_PRINT) !!}</pre></th>
       </tr>

       <tr>
           <th>Data</th>
           <th><pre>{!! json_encode($logError->data, JSON_PRETTY_PRINT) !!}</pre></th>
       </tr>

       <tr>
           <th>Created</th>
           <th>{{ $logError->created_at }}</th>
       </tr>
   </table>
</div>
