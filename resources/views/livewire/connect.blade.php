<div>
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('getAccessTokenError'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    {{ session()->get('getAccessTokenError') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            @endif

            <a href="{{ $urlLogin }}" class="btn btn-primary btn-lg btn-block waves-effect waves-light mb-1">Connect to account facebook</a>
        </div>
    </div>

    <div class="row mt-3">
        @foreach($userSettings as $userSetting)
            <div class="col-md-6 col-xl-3">
                <div wire:loading>
                    <x-loading />
                </div>
                <div class="card text-center">
                    <img class="card-img-top img-fluid" src="{{ get_full_link($userSetting->fb_avatar) }}" alt="Card image cap">
                    <div class="card-body">
                        <h4 class="card-title mt-0 text-center">{{ $userSetting->fb_name }}</h4>
                        <button class="btn btn-danger waves-effect waves-light">Delete</button>
                        <button wire:click="loadPage({{ $userSetting }})" type="button" class="btn btn-dark waves-effect waves-light">
                            @if($isLoadPage)
                                <i class="bx bx-loader bx-spin font-size-16 align-middle mr-2"></i>
                            @endif
                                Load Page
                        </button>
                    </div>
                </div>

            </div><!-- end col -->
        @endforeach
    </div>
</div>
