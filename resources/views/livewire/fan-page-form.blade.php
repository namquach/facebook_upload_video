<div>
    @if(session()->has('create_source_success'))
        <div>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <i class="mdi mdi-check-all mr-2"></i>
                {{ session()->get('create_source_success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
        </div>
    @endif

    @if(session()->has('create_source_error'))
            <div>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <i class="mdi mdi-block-helper mr-2"></i>
                    {{ session()->get('create_source_error') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </div>
    @endif

    <div wire:loading>
        <x-loading />
    </div>
    <form wire:submit.prevent="submit">
        {!! renderInput($errors, 'pageVideo.source_id', ['value' => '', 'label' => 'Source Id', 'wire_model' => 'wire:model']) !!}
        {!! renderSelect($errors, 'pageVideo.type', $typeList, ['key' => 'key', 'value' => 'value', 'label' => 'Type', 'wire_model' => 'wire:model']) !!}
        {!! renderTextarea($errors, 'pageVideo.description', ['label' => 'Description']) !!}
        {!! renderSelect($errors, 'pageVideo.priority', $priority,['key' => 'key', 'value' => 'value', 'label' => 'Priority']) !!}


        <button class="btn btn-success">Save</button>
        <button class="btn btn-primary" type="button" wire:click="resetModel">Rest</button>
        @if(!empty($pageVideo->source_id) && !empty($pageVideo->type))
            @php
                $link = '';
                $error = '';
                switch ($pageVideo->type)
                {
                    case SOURCE_FACEBOOK:
                        $link = \Libs\Facebook\Rest\DownLoad::getLinkFacebook($pageVideo->source_id, $error);
                        break;
                    case SOURCE_YOUTOBE:
                        $link = \Libs\Facebook\Rest\DownLoad::getLinkYouToBe($pageVideo->source_id, $error);
                        break;

                }
            @endphp
            <a href="{{ $link }}" target="_blank" class="btn btn-danger">Test get link</a>
        @endif
    </form>

    @if($pageVideo->_id !== null)
        <br>
        <div class="table-responsive">
            <table class="table table-nowrap table-centered mb-0">
                <tr>
                    <th>Page Id</th>
                    <th>{{ @$pageVideo->fan_page_id }}</th>
                </tr>

                <tr>
                    <th>Status</th>
                    <th>{{ @$pageVideo->status }}</th>
                </tr>

                <tr>
                    <th>Error Download</th>
                    <th>
                        <pre>{!! json_encode(@$pageVideo->error_download) !!}</pre>
                    </th>
                </tr>

                <tr>
                    <th>Error Upload</th>
                    <th>
                        <pre>{!! json_encode(@$pageVideo->error_upload) !!}</pre>
                    </th>
                </tr>
                <tr>
                    <th>Path video</th>
                    <th>{{ @$pageVideo->path_video }}</th>
                </tr>

                <tr>
                    <th>Response Video Id</th>
                    <th>{{ @$pageVideo->response_video_id }}</th>
                </tr>

                <tr>
                    <th>Created</th>
                    <th>{{ @$pageVideo->created_at }}</th>
                </tr>
            </table>
        </div>
    @endif
</div>
