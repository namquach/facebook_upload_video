<div>
    <div wire:loading>
        <x-loading />
    </div>
    <form wire:submit.prevent="submit">
        {!! renderInput($errors, 'facebookPage.total_video_upload_by_day', [
            'value' => $facebookPage->total_video_upload_by_day,
            'label' => 'Total video upload by day',
            'type' => 'number'
        ]) !!}
        {!! renderInput($errors, 'facebookPage.remaining_amount', [
                'value' => $facebookPage->remaining_amount,
                'label' => 'Remaining amount',
                 'type' => 'number'
        ]) !!}
        <a href="{{ route('backend.fan-page.index') }}" type="button" class="btn btn-light">Back</a>
        <button class="btn btn-success">Save</button>
    </form>
</div>
