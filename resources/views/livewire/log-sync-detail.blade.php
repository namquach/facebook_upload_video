<div class="table-responsive">
    <table class="table table-nowrap table-centered mb-0">
        <tr>
            <th>ID</th>
            <th>{{ $logSync->_id }}</th>
        </tr>
        <tr>
            <th>Type</th>
            <th>{{ $logSync->type }}</th>
        </tr>

        <tr>
            <th>Error</th>
            <th><pre>{!! json_encode($logSync->error, JSON_PRETTY_PRINT) !!}</pre></th>
        </tr>
        <tr>
            <th>Data</th>
            <th><pre>{!! json_encode($logSync->data, JSON_PRETTY_PRINT) !!}</pre></th>
        </tr>
        <tr>
            <th>Message</th>
            <th><pre>{!! json_encode($logSync->message, JSON_PRETTY_PRINT) !!}</pre></th>
        </tr>
        <tr>
            <th>Created</th>
            <th>{{ $logSync->created_at }}</th>
        </tr>
    </table>
</div>
