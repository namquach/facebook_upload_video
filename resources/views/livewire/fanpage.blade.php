<div>
    <div class="table-responsive">
        <table class="table project-list-table table-nowrap table-centered table-borderless">
            <thead>
            <tr>
                <th scope="col">Image</th>
                <th scope="col">Name</th>
                <th scope="col">Created</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
                @foreach($fanPages as $fanPage)
                    <tr>
                        <td>
                            <img width="50" height="50" src="{{ get_full_link($fanPage->avatar) }}" alt="">
                        </td>
                        <td>
                            <a href="{{ route('backend.fan-page.detail', ['id' => $fanPage->id]) }}">{{ $fanPage->name }}</a>
                        </td>
                        <td>{{ $fanPage->created_at }}</td>
                        <td>
                            @if ($fanPage->status === \App\Models\Mongo\FacebookPage::status_active)
                                <button wire:click="changeStatus('{{ $fanPage->_id }}', {{ \App\Models\Mongo\FacebookPage::status_block }})" type="button" class="btn btn-danger btn-sm">Block</button>
                            @else
                                <button wire:click="changeStatus('{{ $fanPage->_id }}', {{ \App\Models\Mongo\FacebookPage::status_active }})" type="button" class="btn btn-success btn-sm">Active</button>
                            @endif
                            <a href="{{ route('backend.fan-page.edit', ['id' => $fanPage->_id]) }}" class="btn btn-primary btn-sm">Edit</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
