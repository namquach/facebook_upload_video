<div>
    <div wire:loading>
        <x-loading />
    </div>
    <div class="table-responsive">
        <div class="container">
            <div class="row">
                <div class="col-md-12" style="display: flex">

                </div>
            </div>
        </div>
        <br>
        <table class="table table-nowrap table-centered mb-0">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Type</th>
                <th scope="col">Message</th>
                <th>Created</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($logSyncs as $key => $logSync)
                <tr>
                    <td>{{ getNumberRowTable($logSyncs, $key) }}</td>
                    <td>{{ @$logSync->type }}</td>
                    <td>{{ $logSync->message }}</td>
                    <td>{{ $logSync->created_at }}</td>
                    <td>
                        <a href="{{ route('backend.log-sync.show', ['id' => $logSync->_id]) }}" class="btn btn-primary btn-sm">View</a>
                        <button wire:click="deleteConfirm('{{ $logSync->_id }}')" class="btn btn-danger btn-sm">Delete</button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $logSyncs->links() }}

    </div>
</div>
