<div>
    <div wire:loading>
        <x-loading />
    </div>
    <div class="table-responsive">
        <div class="container">
            <div class="row">
                <div class="col-md-12" style="display: flex">
                    <input wire:model.lazy="search" type="text" class="form-control" name="search" placeholder="Search ..." style="width: 200px">
                    <select wire:model="type" class="form-control ml-1" style="width: auto">
                        <option value="">All type</option>
                        <option value="{{ SOURCE_FACEBOOK }}">{{ SOURCE_FACEBOOK }}</option>
                        <option value="{{ SOURCE_YOUTOBE }}">{{ SOURCE_YOUTOBE }}</option>
                    </select>

                    <select wire:model="status" class="form-control ml-1" style="width: auto">
                        <option value="">All status</option>
                        <option value="{{ status_created }}">Created</option>
                        <option value="{{ status_download }}">Download</option>
                        <option value="{{ status_uploaded }}">Uploaded</option>
                    </select>

                    <select wire:model="filter" class="form-control ml-1" style="width: auto">
                        <option value="source_id">Source Id</option>
                        <option value="type">Type</option>
                        <option value="description">Description</option>
                        <option value="created_at">Created</option>
                    </select>

                    <select wire:model="sort" class="form-control ml-1" style="width: auto">
                        <option value="asc">Asc</option>
                        <option value="desc">Desc</option>
                    </select>

                    <select wire:model="limit" class="form-control ml-1" style="width: auto">
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>

                    <button wire:click="resetFilter()" class="btn btn-primary ml-1">Reset</button>
                </div>
            </div>
        </div>
        <br>
        @if($facebookPageVideoList->count())
            <table class="table table-nowrap table-centered mb-0">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Type</th>
                    <th scope="col">Description</th>
                    <th scope="col">Status</th>
                    <th scope="col">Priority</th>
                    <th scope="col">Created</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                @php
                    $index = $facebookPageVideoList->currentPage();
                    $perPage = $facebookPageVideoList->perPage();
                @endphp
                @foreach($facebookPageVideoList as $key => $item)
                    <tr>
                        <td>{{ getNumberRowTable($facebookPageVideoList, $key) }}</td>
                        <td>{{ $item->type  }}</td>
                        <td>
                            <a target="_blank" href="{{ $item->source_id }}">{{ substrwords($item->description, 50) }}</a>
                        </td>
                        <td class="text-center">
                            @if ($item->status == status_created)
                                <span class="badge badge-pill badge-soft-warning font-size-8">Created</span>
                            @elseif($item->status == status_download)
                                @if(empty($item->error_download))
                                    <span class="badge badge-pill badge-soft-success font-size-8">Download</span>
                                @else
                                    <span class="badge badge-pill badge-soft-danger font-size-8">Error download</span>
                                @endif
                            @else
                                @if(empty($item->error_upload))
                                    <span class="badge badge-pill badge-soft-success font-size-8">Uploaded</span>
                                @else
                                    <span class="badge badge-pill badge-soft-danger font-size-8">Error upload</span>
                                @endif
                            @endif
                        </td>
                        <td class="text-center">
                            @switch(@$item->priority)
                                @case('')
                                        <span>-</span>
                                    @break
                                @case('1')
                                    <span class="badge badge-pill badge-soft-primary font-size-8">low</span>
                                    @break
                                @case('5')
                                    <span class="badge badge-pill badge-soft-warning font-size-8">Medium</span>
                                    @break
                                @case(10)
                                    <span class="badge badge-pill badge-soft-danger font-size-8">High</span>
                                    @break
                            @endswitch
                        </td>
                        <td>{{ $item->created_at }}</td>
                        <td style="width: 120px;">
                            <button wire:click="eventUpdate('{{ $item->_id }}')" class="btn btn-primary btn-sm">View</button>
                            <button wire:click="deleteConfirm('{{ $item->_id }}')" class="btn btn-danger btn-sm">Delete</button>
                        </td>
                    </tr>
                @endforeach


                </tbody>
            </table>
            {{ $facebookPageVideoList->links() }}
        @else
            <div class="alert alert-secondary" role="alert">
                Empty data
            </div>
        @endif
    </div>
</div>
