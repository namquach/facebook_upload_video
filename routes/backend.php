<?php

use Illuminate\Support\Facades\Route;

Route::prefix('admin')->middleware(['auth.login'])->group( function () {

//    Route::get('reset-setup-data', function () {
//        $allSetting = \App\Models\Mongo\Setting::all();
//        foreach ($allSetting as $setting){
//            $setting->delete();
//        }
//
//        $facebookPages = \App\Models\Mongo\FacebookPage::all();
//        foreach ($facebookPages as $facebookPage){
//            $facebookPage->delete();
//        }
//
//        $facebookPageVideos = \App\Models\Mongo\FacebookPageVideo::all();
//        foreach ($facebookPageVideos as $facebookPageVideo){
//            $facebookPageVideo->delete();
//        }
//
//        $logs = \App\Models\Mongo\Log::all();
//        foreach ($logs as $log){
//            $log->delete();
//        }
//
//        $logErrors = \App\Models\Mongo\LogError::all();
//        foreach ($logErrors as $logError){
//            $logError->delete();
//        }
//
//        dd('--- DONE ---');
//
//    });
//
//    Route::get('setup-data-631996', function () {
//        \App\Models\Mongo\Setting::initData();
//        dd('--- DONE ---');
//    });

    Route::get('/', 'DashboardController@index')->name('backend.dashboard.index');

    Route::prefix('connect')->group(function () {
        Route::get('/', 'ConnectController@index')->name('backend.connect.index');
    });

    Route::prefix('fan-page')->group(function () {
        Route::get('/', 'FanPageController@index')->name('backend.fan-page.index');
        Route::get('detail/{id}', 'FanPageController@detail')->name('backend.fan-page.detail');
        Route::get('edit/{id}', 'FanPageController@edit')->name('backend.fan-page.edit');
    });

    Route::get('log-error', 'LogErrorController@index')->name('backend.log-error.index');
    Route::get('log-error/{id}', 'LogErrorController@Show')->name('backend.log-error.show');

    Route::get('log-sync', 'LogSyncController@index')->name('backend.log-sync.index');
    Route::get('log-sync/{id}', 'LogSyncController@Show')->name('backend.log-sync.show');

    Route::get('setting', 'SettingController@index')->name('backend.setting.index');
});

Route::get('connect/callback', 'ConnectController@callback')->name('backend.connect.callback');

