<?php
namespace Libs\Facebook\Rest;

use App\Models\Mongo\Setting;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\DocBlock\Tags\Throws;


class FacebookApi
{
    public static function prepare() {
        $settings = Setting::getDataAuthFB();

        $config = [];
        /* @var Setting $setting **/
        foreach($settings as $setting)
        {
            switch ($setting->meta_key)
            {
                case SETTING_KEY_APP_ID:
                    $config['app_id'] = $setting->meta_value;
                    break;
                case SETTING_KEY_SECRET_KEY:
                    $config['app_secret'] = $setting->meta_value;
                    break;
                case SETTING_KEY_GRAPH_API_VERSION:
                    $config['graph_api_version'] = $setting->meta_value;
                    break;
            }
        }

        return new Facebook($config);
    }

    /**
     * @param string $method
     * @param string $endpoint
     * @param string $accessToken
     * @param array $options
     * @return mixed
     */
    public static function call(string $method, string $endpoint, string $accessToken, array $options = [])
    {
        $facebook = self::prepare();
        switch ($method)
        {
            case 'get':
                return $facebook->get($endpoint, $accessToken);
            case 'post':
                return $facebook->post($endpoint, [],$accessToken, $options);
            default:
                break;
        }
    }

    /**
     * @return string
     */
    public static function renderLoginUrl()
    {
        $facebook = clone self::prepare();
        $helper = $facebook->getRedirectLoginHelper();

        $permission = [
            'email',
            'pages_show_list',
            //'user_videos',
            'public_profile',
            'publish_video',

            // upload video to page
            'pages_manage_posts',
            'pages_read_engagement',
            'pages_show_list',
        ];
        return $helper->getLoginUrl(route('backend.connect.callback'), $permission);
    }

    public static function getAccessToken(Request $request)
    {
        $facebook = clone self::prepare();
        $helper = $facebook->getRedirectLoginHelper();

        if(!empty($request->get('state', null)))
        {
            $helper->getPersistentDataHandler()->set('state', $request->get('state'));
        }
        return $helper->getAccessToken();
    }

    public static function getMe(string $accessToken)
    {
        $me = self::call('get', '/me', $accessToken);
        return $me->getGraphUser();
    }

    public static  function accounts(string $accessToken)
    {
        $response = self::call('get', 'me/accounts', $accessToken)->getBody();

        if($response)
            return json_decode($response, true);
        return null;
    }

    public static function getPicture(int $user_id, string $access_token, int $width = 160, int $height = 160)
    {
        $query = http_build_query([
            'width' => $width,
            'height' => $height
        ]);
        return self::call('get', sprintf('%s/picture?%s', $user_id, $query), $access_token);
    }
}
