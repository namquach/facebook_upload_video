<?php
namespace Libs\Facebook\Rest;

use App\Models\Mongo\FacebookPage;
use App\Models\Mongo\FacebookPageVideo;
use App\Models\Mongo\Log;

class Upload
{
    public static function facebook(FacebookPage $facebookPage, FacebookPageVideo $facebookPageVideo)
    {
        $log = new Log();
        $log->type = 'facebook upload';

        try
        {
            if(empty($facebookPageVideo->path_video)){
                throw new \Exception('Path video not found');
            }



            $url = sprintf('https://graph-video.facebook.com/v11.0/%s/videos', $facebookPage->page_id);
            $response = RestApi::call('post', $url, [
                'query' => [
                    'access_token' => $facebookPage->access_token,
                    'file_url' => str_replace('https', 'http', get_full_link($facebookPageVideo->path_video)),
                    'description' => $facebookPageVideo->description,
                ]
            ]);
            if(!is_string($response)){
                if($response instanceof \Exception){
                    throw new \Exception($response->getMessage());
                }else{
                    throw new \Exception(json_encode($response));
                }

            }

            $response = \GuzzleHttp\json_decode($response, true);

            if(empty($response['id'])){
                throw new \Exception('Upload fail');
            }

            $facebookPageVideo->response_video_id = $response['id'];
            $log->message = 'Upload file success';
            __unset($facebookPageVideo->path_video);

            $facebookPage->remaining_amount = $facebookPage->remaining_amount - 1;
            $facebookPage->save();

        }catch (\Exception $exception){
            $log->error = convertExceptionToArray($exception);
            $log->message = $exception->getMessage();
            $facebookPageVideo->error_upload = $exception->getMessage();
        }

        $log->data = $facebookPageVideo->toArray();
        $log->save();
        $facebookPageVideo->status = status_uploaded;
        $facebookPageVideo->save();
    }
}
