<?php
namespace Libs\Facebook\Rest;

use App\Models\Mongo\Setting;
use Illuminate\Http\Request;

class Graph
{
    protected static $baseUrl = 'https://graph.facebook.com/';

    public static function setUrl($url)
    {
        return self::$baseUrl . trim(ltrim($url, '/'));
    }

    public static function getMe(string $access_token)
    {
        $url = self::setUrl('me');
        return RestApi::call('get', $url, [
            'query' => [
                'access_token' => $access_token
            ]
        ]);
    }

    public static function getPicture(int $user_id, string $access_token, int $width = 160, int $height = 160)
    {
        $url = self::setUrl(sprintf('%d/picture', $user_id));
        return RestApi::call('get', $url, [
            'query' => [
                'access_token' => $access_token,
                'width' => $width,
                'height' => $height,
            ]
        ]);
    }

    public static function getVideoByUser(int $user_id, string $access_token)
    {
        $url = self::setUrl(sprintf('%d/live_videos', $user_id));
        return RestApi::call('get', $url, [
            'query' => [
                'access_token' => $access_token
            ]
        ]);
    }

    public static function getAccessToken(Request $request)
    {
        try {
            $settingAppID = Setting::query()->where('meta_key', SETTING_KEY_APP_ID)->first();
            $settingSecret = Setting::query()->where('meta_key', SETTING_KEY_SECRET_KEY)->first();

            $response = RestApi::call('get', 'https://graph.facebook.com/v9.0/oauth/access_token', [
                'query' => [
                    'client_id' => $settingAppID->meta_value,
                    'redirect_uri' => route('backend.connect.callback'),
                    'client_secret' => $settingSecret->meta_value,
                    'code' => $request->get('code'),
                ]
            ]);

            $request = json_decode($response, true);

            return $request['access_token'];
        }catch (\Exception $exception){

        }

        return '';
    }

    public static function accounts($accessToken)
    {
        try
        {
            $response = RestApi::call('get', 'https://graph.facebook.com/v9.0/me/accounts', [
                'query' => [
                    'access_token' => $accessToken
                ]
            ]);
            return \GuzzleHttp\json_decode($response, true);
        }catch (\Exception $exception){

        }

        return null;
    }
}
