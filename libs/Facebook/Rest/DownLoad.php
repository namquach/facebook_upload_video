<?php
namespace Libs\Facebook\Rest;

use App\Models\Mongo\FacebookPageVideo;
use App\Models\Mongo\Log;
use Libs\Helpers\FileHelper;

class DownLoad
{
    /***
     * @param string $url
     * @param string $error
     * @return string
     */
    public static function getLinkFacebook(string $url, string &$error)
    {
        return self::getLinkFacebookGETFVID($url, $error);
    }

    /**
     * @param string $url
     * @param string $error
     * @return string
     */
    public static function getLinkFacebookFBDOWN(string $url, string &$error)
    {
        $link = '';

        try
        {
            $responseHtml = RestApi::call('post', 'https://fbdown.net/download.php', [
                'form_params' => [
                    'URLz' => $url
                ]
            ]);

            $resolution = 'hdlink';

            $pattern = '/<a id="'. $resolution .'" ?.*>(.*)<\/a>/';
            preg_match($pattern, $responseHtml, $matches);

            if(empty($matches[0]))
            {
                $resolution = 'sdlink';
                $pattern = '/<a id="'. $resolution .'" ?.*>(.*)<\/a>/';
                preg_match($pattern, $responseHtml, $matches);

                if(empty($matches[0])){
                    throw new \Exception('Can not get link sd or hd video');
                }
            }

            $doc = new \DOMDocument;
            if ($doc->loadHTML($matches[0])) {
                $link = $doc->getElementById($resolution)->getAttribute('href');
            }else{
                throw new \Exception('Can not load DOMDocument');
            }

        }catch (\Exception $exception){
            $error = $exception->getMessage();
        }

        return $link;
    }

    /**
     * @param string $url
     * @param string $error
     * @return string
     */
    public static function getLinkFacebookGETFVID(string $url, string &$error)
    {
        $link = '';

        try
        {
            $responseHtml = RestApi::call('post', 'https://www.getfvid.com/vi/downloader', [
                'form_params' => [
                    'url' => $url
                ]
            ]);

            $pattern = '/<a ?.* target="_blank" class="btn btn-download" rel="nofollow" download>(.*)<\/a>/';
            preg_match($pattern, $responseHtml, $matches);

            if(!empty($matches[0]))
            {
                preg_match_all('~<a(.*?)href="([^"]+)"(.*?)>~', $matches[0], $matchesHref);

                foreach($matchesHref as $_item)
                {
                    if(filter_var($_item[0], FILTER_VALIDATE_URL)){
                        $link = isset($_item[0]) ? $_item[0] : '';
                    }
                }

                if(empty($matches[0])){
                    throw new \Exception('Can not get link sd or hd video');
                }
            }else{
                throw new \Exception('Can not get tag a');
            }


        }catch (\Exception $exception){
            $error = $exception->getMessage();
        }

        return $link;
    }

    public static function facebook(FacebookPageVideo $facebookPageVideo)
    {
//        $log = new Log();
//        $log->type = Log::TYPE_FACEBOOK_DOWNLOAD;
//        $error = '';
//
//        try
//        {
//            $linkMp4 = self::getLinkFacebook($facebookPageVideo->source_id, $error);
//        }catch (\Exception $exception){
//            $log->error = convertExceptionToArray($exception);
//            $facebookPageVideo->error_download = $exception->getMessage();
//        }
//
//        $log->data = $facebookPageVideo->toArray();
//        $log->save();
//
//        $facebookPageVideo->status = status_download;
//        $facebookPageVideo->save();
    }

    private static function resolution()
    {
        return [
            '240p',
            '1440p',
            '1080p',
            '720p',
            '480p',
            '360p',
            '240p'
        ];
    }

    public static function old_0001_youtobe($source_id)
    {
        $log = new Log();
        $log->type = Log::TYPE_YOUTOBE_DOWNLOAD;

        try
        {
            $url = base_url_view_youtobe . $source_id;

            $response = RestApi::call('post', 'https://yt1s.com/api/ajaxSearch/index', [
                'form_params' => [
                    'vt' => 'home',
                    'q' => $url
                ]
            ]);

            $response = \GuzzleHttp\json_decode($response, true);

            if(empty($response['links']['mp4']) | count($response['links']['mp4']) === 0){
                throw new \Exception('Can not found source video with link %s', $url);
            }

            /***
             * $item = [
             *      size => ...,
             *      f    => mp4, ...
             *      q   => 720p, 360p, ...
             *      k   => key get link
             * ]
             */
            $resolution = [];
            $key = 0;
            foreach($response['links']['mp4'] as $item)
            {
                if($key === 0) {
                    $resolution = $item;
                    $key++;
                    continue;
                }
                if((int)$item['q'] > (int)$resolution['q']){
                    $resolution = $item;
                }

                $key++;
            }

            if(empty($resolution['k'])){
                throw new \Exception('Get resolution resource not found with link %s' , $url);
            }

            $responseVideo = RestApi::call('post', 'https://yt1s.com/api/ajaxConvert/convert', [
                'form_params' => [
                    'vid' => $source_id,
                    'k' => $resolution['k']
                ],
                'headers' => [
                    'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36'
                ]
            ]);

            if(!is_string($responseVideo)){
                throw new \Exception(sprintf('Get responseVideo resource not found with vid %s and k %s' , $source_id, $resolution['k']));
            }
            $responseVideo = \GuzzleHttp\json_decode($responseVideo);
            //dd(file_get_contents($responseVideo->dlink));

            //header("Content-type: video/flv");
            //header("Content-Disposition:attachment;filename=video.mp4");

            echo '<a href="'. $responseVideo->dlink .'">click</a>';
            dd($responseVideo);
            dd('');
        }catch (\Exception $exception){
            dd($exception->getMessage(), $exception->getLine());
        }

    }

    /***
     * @param string $url
     * @param string $error
     * @return string
     */
    public static function getLinkYouToBe(string $url,string &$error)
    {
        $link = '';
        try
        {
            $htmlResponse = RestApi::call('post', 'https://getvideo.id/get_video', [
                'form_params' => [
                    'url' => $url,
                    'ajax' => 1,
                ]
            ]);

            $pattern = '/<a (.*) download="(.*).mp4">(.*)<\/a>/';

            preg_match($pattern, $htmlResponse, $matches);

            if(!empty($matches[0]))
            {
                //preg_match('/href=(["\'])([^\1]*)\1/i', $matches[0], $matchesHref);
                preg_match_all('~<a(.*?)href="([^"]+)"(.*?)>~', $matches[0], $matchesHref);

                foreach($matchesHref as $_item)
                {
                    if(filter_var($_item[0], FILTER_VALIDATE_URL)){
                        $link = isset($_item[0]) ? $_item[0] : '';
                    }
                }

            }else {
                $error = 'Can not get tag a with link video';
            }

        }catch (\Exception $exception){
            $error = $exception->getMessage();
        }

        return $link;
    }

    public static function youtobe(FacebookPageVideo $facebookPageVideo)
    {
        $error = '';
        $linkMp4 = self::getLinkYouToBe($facebookPageVideo->source_id, $error);

        $log = new Log();
        $log->type = Log::TYPE_YOUTOBE_DOWNLOAD;

        $linkUpload = FileHelper::createLink(PATH_UPLOAD_VIDEO, $facebookPageVideo->_id . 'mp4');

        try {
            if(!empty($linkMp4) &&  FileHelper::saveFile($linkUpload, file_get_contents($linkMp4)) !== false){
                $facebookPageVideo->path_video = $linkUpload;
            }else{
                throw new \Exception('Can not put file to server or error ' . $error );
            }
        }catch (\Exception $exception){
            $log->error = convertExceptionToArray($exception);
            $facebookPageVideo->error_download = $exception->getMessage();
        }

        $log->data = $facebookPageVideo->toArray();

        $facebookPageVideo->status = status_download;
        $facebookPageVideo->save();
        $log->save();
    }
}
