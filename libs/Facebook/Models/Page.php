<?php
namespace Libs\Facebook\Models;

use Libs\Facebook\Components\Paging;

class Page
{
    /** @var array $data */
    public $data;

    /* @var Paging $paging **/
    public $paging;

    public function __construct(array $page = [])
    {
        if(isset($page['data'])) $this->data = $page['data'];
        if(isset($page['paging'])) $this->paging = $page['paging'];
    }
}
