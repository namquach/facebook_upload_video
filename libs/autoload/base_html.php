<?php

if(!function_exists('parseAttributeToString'))
{
    function parseAttributeToString(string $attribute) : string
    {
        $attribute = str_replace('_', ' ', $attribute);
        return ucwords($attribute);
    }
}

if(!function_exists('renderInput'))
{
    function renderInput($errors, string $attribute, array $options = [])
    {
        if(empty($options['type'])) $options['type'] = 'text';
        if(empty($options['label'])) $options['label'] = parseAttributeToString($attribute);

        $errorMgs = '';
        if($errors->has($attribute)){
            $errorMgs = '
            <ul class="parsley-errors-list filled">
                <li class="parsley-required">'. $errors->first($attribute) .'</li>
            </ul>
        ';
        }

        if(empty($options['wire_model'])) $options['wire_model'] = "wire:model.defer";

        $options['value'] = old($attribute) !== null ? old($attribute) : $options['value'];

        return '
             <div class="form-group">
                <label>'. $options['label'] .'</label>
                <div>
                    <input
                        '. $options['wire_model'] .'="'. $attribute .'"
                        type="'. $options['type'] .'"
                        class="form-control '. ( $errors->has($attribute) ? 'parsley-error' : '' ) .'"
                        name="'. $attribute .'"
                        id="'. $attribute .'"
                        value="'. $options['value'] .'"
                    />
                </div>
                '. $errorMgs .'
            </div>
        ';
    }
}

if(!function_exists('renderSelect'))
{
    function renderSelect($errors, string $attribute, $items, array $options)
    {
        if (empty($options['label'])) $options['label'] = parseAttributeToString($attribute);
        if(empty($options['selectEvent'])) $options['selectEvent'] = '';

        $htmlOptions = '';
        if (empty($options['selected'])) $options['selected'] = '';
        if (!empty(old($attribute))) $options['selected'] = old($attribute);

        if(count($items))
        {
            foreach ($items as $item) {
                if (is_array($item)) $item = (object)$item;

                if ($item->{$options['key']} == $options['selected'])
                    $htmlOptions .= '<option value="' . $item->{$options['key']} . '" selected="selected">' . $item->{$options['value']} . '</option>';
                else
                    $htmlOptions .= '<option value="' . $item->{$options['key']} . '">' . $item->{$options['value']} . '</option>';
            }
        }

        $errorMgs = '';
        if ($errors->has($attribute)) {
            $errorMgs = '
            <ul class="parsley-errors-list filled">
                <li class="parsley-required">' . $errors->first($attribute) . '</li>
            </ul>
        ';
        }

        $selectClass = 'form-control ';

        if(!empty($options['selectClass'])) $selectClass .= $options['selectClass'];

        if(empty($options['wire_model'])) $options['wire_model'] = "wire:model.defer";

        return '
            <div class="form-group">
                <label>' . $options['label'] . '</label>
                <select
                    class="'. $selectClass .'"
                    '. $options['wire_model'] .'="'. $attribute .'" '. $options['selectEvent'] .'
                    name="'. $attribute .'"
                    id="'. str_replace('.', '_', $attribute) .'"
                >
                    ' . $htmlOptions . '
                </select>
                ' . $errorMgs . '
            </div>
        ';
    }
}

if(!function_exists('renderTextarea'))
{
    function renderTextarea($errors, string $attribute, array $options = [])
    {
        if(empty($options['type'])) $options['type'] = 'text';
        if(empty($options['label'])) $options['label'] = parseAttributeToString($attribute);

        $errorMgs = '';
        if($errors->has($attribute)){
            $errorMgs = '
            <ul class="parsley-errors-list filled">
                <li class="parsley-required">'. $errors->first($attribute) .'</li>
            </ul>
        ';
        }

        return '
             <div class="form-group">
                <label>'. $options['label'] .'</label>
                <div>
                    <textarea wire:model.defer="'. $attribute .'" class="form-control '. ( $errors->has($attribute) ? 'parsley-error' : '' ) .'"></textarea>
                </div>
                '. $errorMgs .'
            </div>
        ';
    }
}



if(!function_exists('getLinkImageStorage'))
{
    function getLinkImageStorage(string $path)
    {
        return str_replace( 'public', '', url('/')) . 'storage/app/' . $path;
    }
}
