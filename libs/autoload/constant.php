<?php
const SETTING_KEY_APP_ID = 'app_id';
const SETTING_KEY_SECRET_KEY = 'secret_key';
const SETTING_KEY_GRAPH_API_VERSION = 'fb_graph_api_version';
const SETTING_KEY_TOTAl_UPLOAD_BY_DAY = 'total_video_upload_by_day';
const SETTING_KEY_REMAINING_AMOUNT = 'remaining_amount';

const ASSETS_IMAGE_NOT_FOUND = 'public/backend/assets/images/default/not_found_image.png';
const PATH_UPLOAD_AVATAR = 'public/backend/assets/images/avatars/';
const PATH_UPLOAD_VIDEO = 'public/video/';
const SOURCE_FACEBOOK = 'Facebook';
const SOURCE_YOUTOBE = 'Youtobe';

// facebook fan page video status
const status_created = 0;
const status_download = 5;
const status_uploaded = 10;

const base_url_view_youtobe = 'https://www.youtube.com/watch?v=';
