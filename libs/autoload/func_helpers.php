<?php

if(!function_exists('get_full_link'))
{
    function get_full_link ($path) {
        return str_replace('public', '', url('/')) . $path;
    }
}

if(!function_exists('__unset'))
{
    function __unset($path)
    {
        $path = base_path() . $path;
        if(file_exists($path)){
            unset($path);
        }
    }
}

if(!function_exists('substrwords'))
{
    function substrwords($text, $maxchar, $end='...') {

        if (strlen($text) > $maxchar || $text == '') {
            $words = preg_split('/\s/', $text);
            $output = '';
            $i      = 0;
            while (1) {
                $length = strlen($output)+strlen($words[$i]);
                if ($length > $maxchar) {
                    break;
                }
                else {
                    $output .= " " . $words[$i];
                    ++$i;
                }
            }
            $output .= $end;
        }
        else {
            $output = $text;
        }
        return $output;
    }
}

if(!function_exists('convertExceptionToArray'))
{
    function convertExceptionToArray($exception)
    {
        return [
            'message' => $exception->getMessage(),
            'code' => $exception->getCode(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'trace_as_string' => $exception->getTraceAsString(),
            'trace' => $exception->getTrace(),
            'previous' => $exception->getPrevious()
        ];
    }
}

if(!function_exists('getNumberRowTable'))
{
    function getNumberRowTable(\Illuminate\Pagination\LengthAwarePaginator $paginator, int $index)
    {
        return $index + 1 + ($paginator->currentPage() - 1) * $paginator->perPage();
    }
}

