<?php
namespace Libs\Helpers;

class FileHelper
{
    public static function createFullPathUrlUpload($path)
    {
        return url('/') . $path;
    }

    public static function createLink($folder, $name){

        $folder = sprintf('%s/%s/%s', rtrim($folder, '/'), date('Y'), date('m'));

        if(!is_dir(base_path() . '/' . $folder)){
            mkdir(base_path() . '/' . $folder, 0777, true);
        }

        return $folder . '/' . $name;
    }

    public static function saveFile(string $path, string $content)
    {
        return file_put_contents(base_path() . '/' . $path, $content);
    }

    public static function __unlink(string $path)
    {
        @unlink(base_path() . '/' . $path);
    }

}
