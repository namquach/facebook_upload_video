<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Quach Hoai Nam';
        $user->email = 'qhnam.67@gmail.com';
        $user->password = \Hash::make('nam631996');
        $user->save();
    }
}
