<?php

namespace App\Console\Commands;

use App\Models\Mongo\FacebookPage;
use App\Models\Mongo\Setting;
use Hamcrest\Core\Set;
use Illuminate\Console\Command;

class ResetTotalVideoUploadCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'setting-reset-total-video-upload:reset';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $pages = FacebookPage::query()->where('status', FacebookPage::status_active)->get();

        if ($pages->count() > 0) {
            /** @var FacebookPage $page */
            foreach($pages as $page)
            {
                $page->remaining_amount = $page->total_video_upload_by_day;
                $page->save();
            }
        }

        return 0;
    }
}
