<?php

namespace App\Console\Commands;

use App\Models\Mongo\FacebookPageVideo;
use App\Models\Mongo\Log;
use App\Models\Mongo\LogError;
use Illuminate\Console\Command;
use Libs\Facebook\Rest\DownLoad;
use Libs\Helpers\FileHelper;

class FaceBookPageVideoDownloadCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'facebook-page-video:download';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This is cron down load video from internet to server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            /** @var FacebookPageVideo $item */
            $item = FacebookPageVideo::query()->where('status', status_created)
                ->orderBy('created_at', 'asc')
                ->orderByDesc('priority')
                ->first();

            if($item === null) return 1;

            $linkMp4 = '';
            $error = '';

            $log = new Log();

            switch ($item->type)
            {
                case SOURCE_FACEBOOK:
                    $linkMp4 = DownLoad::getLinkFacebook($item->source_id, $error);
                    $log->type = Log::TYPE_FACEBOOK_DOWNLOAD;
                    break;

                case SOURCE_YOUTOBE:
                    $linkMp4 = DownLoad::getLinkYouToBe($item->source_id, $error);
                    $log->type = Log::TYPE_YOUTOBE_DOWNLOAD;
                    break;

                default:
                    break;
            }

            try
            {
                $linkUpload = FileHelper::createLink(PATH_UPLOAD_VIDEO, $item->_id . '.mp4');

                if(!empty($linkMp4) && FileHelper::saveFile($linkUpload, file_get_contents($linkMp4)) !== false)
                {
                    $item->path_video = $linkUpload;
                }else{
                    throw new \Exception('Can not put file to server or error ' . $error );
                }

                $log->message = 'Download file success';
            }catch (\Exception $e){
                $log->error = convertExceptionToArray($e);
                $log->message = $e->getMessage();
                $item->error_download = $e->getMessage();
            }

            $log->data = $item->toArray();

            $item->status = status_download;
            $item->save();
            $log->save();
        }catch (\Exception $exception){
            LogError::logException($exception, []);
        }

        return 0;
    }
}
