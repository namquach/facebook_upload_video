<?php

namespace App\Console\Commands;

use App\Models\Mongo\FacebookPage;
use App\Models\Mongo\FacebookPageVideo;
use App\Models\Mongo\LogError;
use Illuminate\Console\Command;
use Libs\Facebook\Rest\Upload;

class FacebookPageVideoUploadCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'facebook-page-video:upload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This is cron upload video server to facebook';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $facebookPages = FacebookPage::query()->where('status', FacebookPage::status_active)->get();

            foreach ($facebookPages as $facebookPage)
            {
                /** @var FacebookPageVideo $item */
                $item = FacebookPageVideo::query()->where('fan_page_id', $facebookPage->page_id)
                    ->where('status', status_download)
                    ->orderBy('created_at', 'asc')
                    ->orderByDesc('priority')
                    ->first();

                if($item !== null && $facebookPage->remaining_amount > 0)
                {
                    Upload::facebook($facebookPage, $item);
                }
            }
        }catch (\Exception $exception){
            LogError::logException($exception, []);
        }

        return 0;
    }
}
