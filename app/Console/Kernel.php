<?php

namespace App\Console;

use App\Console\Commands\FaceBookPageVideoDownloadCommand;
use App\Console\Commands\FacebookPageVideoUploadCommand;
use App\Console\Commands\ResetTotalVideoUploadCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        FaceBookPageVideoDownloadCommand::class,
        FacebookPageVideoUploadCommand::class,
        ResetTotalVideoUploadCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('facebook-page-video:download')->everyFiveMinutes()->withoutOverlapping()->appendOutputTo('/var/www/html/funny/download.txt')->runInBackground();
        $schedule->command('facebook-page-video:upload')->hourly()->withoutOverlapping()->appendOutputTo('/var/www/html/funny/upload.txt')->runInBackground();
        $schedule->command('setting-reset-total-video-upload:reset')->dailyAt('05:00')->runInBackground();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
