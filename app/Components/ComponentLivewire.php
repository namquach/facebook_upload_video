<?php
namespace App\Components;

use Livewire\Component;
use Livewire\WithPagination;

class ComponentLivewire extends Component
{
    use WithPagination;

    const ALERTIFY_WARNING = 'warning';
    const ALERTIFY_SUCCESS = 'success';
    const ALERTIFY_ERROR = 'error';

    public function paginationView()
    {
        return 'vendor.livewire.custom';
    }

    public function dispatchAlertify($type, $message)
    {
        $this->dispatchBrowserEvent('alertify', [
            'type' => $type,
            'message' => $message,
        ]);
    }

    public function deleteConfirm($id)
    {
        $this->dispatchBrowserEvent('swal.confirm', [
            'type' => 'question',
            'title' => 'Are you sure ?',
            'id' => $id,
            'text' => '',
            'icon' => 'question',
            'action' => 'delete'
        ]);
    }
}
