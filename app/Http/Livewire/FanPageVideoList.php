<?php

namespace App\Http\Livewire;

use App\Components\ComponentLivewire;
use App\Models\Mongo\FacebookPageVideo;

class FanPageVideoList extends ComponentLivewire
{
    public $facebookPage;

    public $listeners = [
        'reloadFanPageVideoList' => '$refresh',
        'delete'
    ];

    public $search;
    public $type;
    public $status;
    public $filter;
    public $sort;
    public $limit = 10;
    public function render()
    {
        return view('livewire.fan-page-video-list', [
            'facebookPageVideoList' => FacebookPageVideo::search(
                $this->search,
                $this->type,
                $this->status,
                $this->filter,
                $this->sort
            )->paginate((int)$this->limit),
        ]);
    }

    public function resetFilter()
    {
        $this->reset([
            'search',
            'type',
            'status',
            'filter',
            'sort',
            'limit'
        ]);

        $this->resetPage();
    }

    public function delete($id)
    {
        /** @var FacebookPageVideo $model */
        $model = FacebookPageVideo::query()->where('_id',$id)->first();
        if($model && $model->status === status_created){
            $model->delete();
            $this->dispatchAlertify(self::ALERTIFY_SUCCESS, 'Delete video success');
        }
    }

    public function eventUpdate($facebookPageVideoId)
    {
        $this->emit('eventUpdateFacebookPageVideo', $facebookPageVideoId);
    }

    public function getQueryString()
    {
        return [];
    }
}
