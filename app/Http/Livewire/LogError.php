<?php

namespace App\Http\Livewire;

use App\Components\ComponentLivewire;

class LogError extends ComponentLivewire
{
    public function render()
    {
        return view('livewire.log-error', [
            'logErrors' => \App\Models\Mongo\LogError::query()->orderByDesc('created_at')->paginate(20),
        ]);
    }

    public function getQueryString()
    {
        return [];
    }
}
