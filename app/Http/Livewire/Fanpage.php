<?php

namespace App\Http\Livewire;

use App\Components\ComponentLivewire;
use App\Models\Mongo\FacebookPage;

class Fanpage extends ComponentLivewire
{
    public $listeners = ['eventChangeStatus'];

    public function render()
    {
        return view('livewire.fanpage', [
            'fanPages' => FacebookPage::search('')->simplePaginate(10),
        ]);
    }

    public function changeStatus($id, $status)
    {
        $this->dispatchBrowserEvent('swal.confirm', [
            'type' => 'question',
            'title' => 'Are you sure ?',
            'id' => ['id' => $id,'status' => $status],
            'text' => '',
            'icon' => 'question',
            'action' => 'eventChangeStatus',
        ]);
    }

    public function eventChangeStatus($id)
    {
        $fanPage = FacebookPage::query()->where('_id', $id['id'])->first();

        if($fanPage){
            $fanPage->status = $id['status'];
            $fanPage->save();
            $this->dispatchAlertify('success', 'Update status success');
        }
    }
}
