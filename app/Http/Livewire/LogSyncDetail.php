<?php

namespace App\Http\Livewire;

use App\Components\ComponentLivewire;
use App\Models\Mongo\Log;

class LogSyncDetail extends ComponentLivewire
{
    /** @var Log $logSync */
    public $logSync;

    public function render()
    {
        return view('livewire.log-sync-detail');
    }
}
