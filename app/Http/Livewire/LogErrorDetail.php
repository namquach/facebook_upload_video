<?php

namespace App\Http\Livewire;

use App\Components\ComponentLivewire;

class LogErrorDetail extends ComponentLivewire
{
    /** @var \App\Models\Mongo\LogError $logError */
    public $logError;

    public function render()
    {
        return view('livewire.log-error-detail');
    }
}
