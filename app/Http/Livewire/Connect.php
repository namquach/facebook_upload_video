<?php

namespace App\Http\Livewire;

use App\Components\ComponentLivewire;
use App\Models\Mongo\FacebookPage;
use App\Models\UserSetting;
use Libs\Facebook\Rest\FacebookApi;

class Connect extends ComponentLivewire
{
    public $isLoadPage = false;

    public function render()
    {
        return view('livewire.connect', [
            'userSettings' => UserSetting::all(),
            'urlLogin' => FacebookApi::renderLoginUrl(),
        ]);
    }

    public function loadPage(UserSetting $userSetting)
    {
        $this->isLoadPage = true;
        FacebookPage::savePage($userSetting, null);
        $this->isLoadPage = false;
    }
}
