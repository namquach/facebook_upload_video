<?php

namespace App\Http\Livewire;

use App\Models\Mongo\FacebookPage;
use Livewire\Component;

class FanPageEdit extends Component
{
    /** @var FacebookPage $facebookPage */
    public $facebookPage;

    public function rules()
    {
        return [
            'facebookPage.remaining_amount' => 'required',
            'facebookPage.total_video_upload_by_day' => 'required',
        ];
    }

    public function render()
    {
        return view('livewire.fan-page-edit');
    }

    public function submit()
    {
        $this->validate();
        $this->facebookPage->save();
        return redirect()->route('backend.fan-page.index');
    }
}
