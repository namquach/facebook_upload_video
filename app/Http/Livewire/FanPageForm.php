<?php

namespace App\Http\Livewire;

use App\Components\ComponentLivewire;
use App\Models\Mongo\FacebookPage;
use App\Models\Mongo\FacebookPageVideo;

class FanPageForm extends ComponentLivewire
{
    /** @var FacebookPageVideo $pageVideo */
    public $pageVideo;

    /** @var FacebookPage $facebookPage */
    public $facebookPage;

    public $priority = [
        ['key' => 0, 'value' => ''],
        ['key' => 10, 'value' => 'High'],
        ['key' => 5, 'value' => 'Medium'],
        ['key' => 1, 'value' => 'Low'],

    ];

    public $listeners = [
        'eventUpdateFacebookPageVideo'
    ];

    public function rules()
    {
        return [
            'pageVideo.source_id' => 'required',
            'pageVideo.type' => 'required',
            'pageVideo.description' => 'required|max:150',
            'pageVideo.priority' => '',
        ];
    }

    public $typeList = [
        ['key' => '', 'value' => ''],
        ['key' => SOURCE_FACEBOOK, 'value' => SOURCE_FACEBOOK],
        ['key' => SOURCE_YOUTOBE, 'value' => SOURCE_YOUTOBE],
    ];

    public function render()
    {
        return view('livewire.fan-page-form');
    }

    public function resetModel(){
        $this->pageVideo = new FacebookPageVideo();
    }

    public function submit()
    {
        $this->validate();

        if($this->pageVideo->_id === null)
        {
            $this->pageVideo->status = status_created;

            if(FacebookPageVideo::checkExist($this->pageVideo->source_id, $this->pageVideo->type)){
                session()->flash('create_source_error', 'Item exist');
                return false;
            }

        }
        $this->pageVideo->fan_page_id = $this->facebookPage->page_id;
        $this->pageVideo->priority = (int)$this->pageVideo->priority;
        $this->pageVideo->save();
        $this->pageVideo = new FacebookPageVideo();
        session()->flash('create_source_success', 'Create item successfully');
        $this->emit('reloadFanPageVideoList');
    }

    public function eventUpdateFacebookPageVideo($facebookPageVideoId)
    {
        $this->pageVideo = FacebookPageVideo::query()->where('_id', $facebookPageVideoId)->first();
    }
}
