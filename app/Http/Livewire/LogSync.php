<?php

namespace App\Http\Livewire;

use App\Components\ComponentLivewire;
use App\Models\Mongo\Log;

class LogSync extends ComponentLivewire
{
    public function render()
    {
        return view('livewire.log-sync', [
            'logSyncs' => Log::query()->orderByDesc('created_at')->paginate(20),
        ]);
    }

    public function getQueryString()
    {
        return [];
    }
}
