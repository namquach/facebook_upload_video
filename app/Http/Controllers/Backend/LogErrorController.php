<?php

namespace App\Http\Controllers\Backend;


use App\Components\Controller;
use App\Models\Mongo\LogError;

class LogErrorController extends Controller
{
    public function index()
    {
        return view('backend.log-error.index');
    }

    public function show($id)
    {
        return view('backend.log-error.show', [
            'logError' => LogError::query()->findOrFail(['_id' => $id])->first()
        ]);
    }
}
