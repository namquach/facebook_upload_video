<?php

namespace App\Http\Controllers\Backend;

use App\Components\Controller;
use App\Models\Mongo\FacebookPageVideo;
use App\Models\UserSetting;
use Illuminate\Http\Request;
use Libs\Facebook\Rest\DownLoad;
use Libs\Facebook\Rest\Graph;
use Libs\Facebook\Rest\RestApi;
use Libs\Helpers\FileHelper;

class ConnectController extends Controller
{
    public function index()
    {
        return view('backend.connect.index');
    }

    public function callback(Request $request) {

        try
        {
            $isError = false;

            //$accessToken = FacebookApi::getAccessToken($request);
            $accessToken = Graph::getAccessToken($request);
            if(empty($accessToken))
            {
                $isError = true;
            }
            else
            {
                /**
                 * $me::class
                 * @property string name
                 * @property int $id
                 */
                //$me = FacebookApi::getMe($accessToken->getValue());
                $me = Graph::getMe($accessToken);
                if($me === false)
                {
                    $isError = true;
                }
                else
                {
                    $me = json_decode($me);
                    $avatar = Graph::getPicture($me->id, $accessToken);
                    $path = FileHelper::createLink(PATH_UPLOAD_AVATAR, $me->id . '.jpg');

                    FileHelper::saveFile($path, $avatar);
                    //\Storage::disk('local')->put( $path, $avatar);

                    UserSetting::query()->updateOrCreate(
                        ['user_id' => \Auth::user()->id, 'fb_account_id' => $me->id],
                        [
                            'fb_access_token' => $accessToken,
                            'fb_account_id' => $me->id,
                            'fb_avatar' => $path,
                            'fb_name' => $me->name
                        ],
                    );
                }
            }

            if($isError)
            {
                session()->flash('getAccessTokenError', 'Login fail, please tries again');
            }
        }catch (\Exception $exception){
            dd($exception);
            session()->flash('getAccessTokenError', $exception->getMessage() . ' Line: ' . $exception->getLine() . ' File: ' . $exception->getFile() );
        }

        return redirect()->route('backend.connect.index');
    }
}
