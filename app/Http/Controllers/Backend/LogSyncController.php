<?php

namespace App\Http\Controllers\Backend;

use App\Components\Controller;
use App\Models\Mongo\Log;

class LogSyncController extends Controller
{
    public function index()
    {
        return view('backend.log-sync.index');
    }

    public function show($id)
    {
        return view('backend.log-sync.show', [
            'logSync' => Log::query()->findOrFail(['_id' => $id])->first()
        ]);
    }
}
