<?php
namespace App\Http\Controllers\Backend;

use App\Components\BackendController;
use App\Models\Mongo\FacebookPage;

class FanPageController extends BackendController
{
    public function index()
    {
        return view('backend.fanpage.index', );
    }

    public function detail($id)
    {
        return view('backend.fanpage.detail', [
            'facebookPage' => FacebookPage::query()->findOrFail(['page_id' => $id])->first(),
        ]);
    }

    public function edit($id)
    {
        return view('backend.fanpage.edit', [
            'facebookPage' => FacebookPage::query()->findOrFail(['_id' => $id])->first(),
        ]);
    }
}
