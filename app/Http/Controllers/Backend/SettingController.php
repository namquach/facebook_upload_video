<?php

namespace App\Http\Controllers\Backend;

use App\Components\BackendController;
use App\Models\Mongo\Setting;

class SettingController extends BackendController
{
    public function index()
    {
        return view('backend.setting.index', [
            'settings' => Setting::all(),
        ]);
    }
}
