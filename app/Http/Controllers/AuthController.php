<?php
namespace App\Http\Controllers;

use App\Components\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function authenticate(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if(\Auth::attempt($request->only(['email', 'password']))){
            return redirect()->to(route('backend.dashboard.index'));
        }

        $validator = Validator::make([], []);
        $validator->errors()->add('username', 'Username or password invalid');

        return redirect()->route('auth.login')->withInput()->withErrors($validator);
    }
}
