<?php
namespace App\Models\Mongo;

use App\Components\ModelMongo;

/***
 * Class Setting
 * @package App\Models\Mongo
 * @property string $_id
 * @property string $meta_key
 * @property string $meta_value
 */
class Setting extends ModelMongo
{
    protected $collection = 'settings';

    protected $primaryKey = '_id';

    protected $fillable = [
        'meta_key',
        'meta_value'
    ];

    public static function getDataAuthFB()
    {
        return self::query()->whereIn('meta_key', [SETTING_KEY_APP_ID, SETTING_KEY_SECRET_KEY, SETTING_KEY_GRAPH_API_VERSION])->get();
    }

    public static function initData()
    {
        $settingData = [
            ['meta_key' => SETTING_KEY_APP_ID, 'meta_value' => '1180069925839826'],
            ['meta_key' => SETTING_KEY_SECRET_KEY, 'meta_value' => 'd7c0da80cb9c9cd5748caf4bcc51cd55'],
            ['meta_key' => SETTING_KEY_GRAPH_API_VERSION, 'meta_value' => 'v9.0'],
            ['meta_key' => SETTING_KEY_TOTAl_UPLOAD_BY_DAY, 'meta_value' => 10],
            ['meta_key' =>  SETTING_KEY_REMAINING_AMOUNT, 'meta_value' => 10],
        ];

        foreach($settingData as $item)
        {
            $model = self::query()->where('meta_key', $item['meta_key'])->first();
            if(!$model){
                self::create($item);
            }
        }
    }
}
