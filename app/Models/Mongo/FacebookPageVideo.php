<?php
namespace App\Models\Mongo;

use App\Components\ModelMongo;

/***
 * Class FacebookPageVideo
 * @package App\Models\Mongo
 * @property string $_id
 * @property string $source_id
 * @property string $fan_page_id
 * @property string $type
 * @property string $description
 * @property int $status
 * @property string $error_download
 * @property string $path_video
 * @property string $error_upload
 * @property string $response_video_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $priority
 */
class FacebookPageVideo extends ModelMongo
{
    protected $collection = 'page_videos';

    public static function statusList()
    {
        return [
            0 => 'Created',
            5 => 'Download video',
            10 => 'Uploaded',
        ];
    }

    public static function search($search = '',$type = '', $status = '', $filter = 'source_id', $sort = 'asc')
    {
        $query = self::query();

        if(!empty($search)) $query->where('source_id', 'like', '%' . $search . '%')
                                    ->orWhere('description', 'like', '%' . $search . '%');

        if(!empty($type)) $query->where('type', $type);
        if(!empty($status)) $query->where('status', $status);

        if(!empty($filter) && !empty($sort))
        {
            $query->orderBy($filter, $sort);
        }

        return $query;
    }

    public static function checkExist($source_id, $type)
    {
        return self::query()->where([
            'source_id' => $source_id,
            'type' => $type,
        ])->first() === null ? false : true;
    }
}
