<?php
namespace App\Models\Mongo;

use App\Components\ModelMongo;

class LogError extends ModelMongo
{
    protected $collection = 'log_errors';

    //public $fillable = [
    //];

    protected $primaryKey = '_id';

    protected $guarded = ['_id'];

    public static function logException($exception, array $data = [])
    {
        //if(empty($exception->getMessage())) return true;
        self::create([
            '_id' => time(),
            'exception' => convertExceptionToArray($exception),
            'data' => $data
        ]);
    }
}
