<?php
namespace App\Models\Mongo;

use App\Components\ModelMongo;
use App\Models\UserSetting;
use Libs\Facebook\Components\Paging;
use Libs\Facebook\Rest\FacebookApi;
use Libs\Facebook\Rest\Graph;
use Libs\Helpers\FileHelper;

/***
 * Class FacebookPage
 * @package App\Models\Mongo
 * @property string $_id
 * @property int $page_id
 * @property int $account_id
 * @property string $avatar
 * @property string $access_token
 * @property string $category
 * @property array $category_list
 * @property array $tasks
 * @property string $created_at
 * @property string $updated_at
 * @property int $status
 * @property int $total_video_upload_by_day
 * @property int $remaining_amount
 */
class FacebookPage extends ModelMongo
{
    const status_active = 10;
    const status_block = 5;

    protected $collection = 'facebook_pages';

    public static function search(string $search){
        return empty($search) ? self::query() : self::query()->where('name', 'like', '%' . $search . '%');
    }

    public static function savePage(UserSetting $userSetting, $response = null)
    {
        if($response === null){
            $response = Graph::accounts($userSetting->fb_access_token);
        }

        if(!$response) return null;

        $data = isset($response['data']) ? $response['data'] : [];
        $paging = isset($response['paging']) ? $response['data'] : [];

        if(count($data) === 0) return null;

        foreach($data as $key => $pageItem)
        {
            $modelFaceBookPage = FacebookPage::query()->where('page_id', $pageItem['id'])->first();

            if($modelFaceBookPage === null)
            {
                $modelFaceBookPage = new FacebookPage();
                $modelFaceBookPage->page_id = $pageItem['id'];
            }
            $modelFaceBookPage->account_id = $userSetting->fb_account_id;
            $modelFaceBookPage->access_token = $pageItem['access_token'];
            $modelFaceBookPage->category = $pageItem['category'];
            $modelFaceBookPage->name = $pageItem['name'];
            $modelFaceBookPage->category_list = $pageItem['category_list'];
            $modelFaceBookPage->tasks = $pageItem['tasks'];
            $modelFaceBookPage->status = self::status_active;
            $modelFaceBookPage->total_video_upload_by_day = 10; //set default
            $modelFaceBookPage->remaining_amount = 10; // set default
            if($modelFaceBookPage->save())
            {
                if(!empty($modelFaceBookPage->avatar)) FileHelper::__unlink($modelFaceBookPage->avatar);

                $contentFile = Graph::getPicture($modelFaceBookPage->page_id, $userSetting->fb_access_token);

                if(is_string($contentFile))
                {
                    $path = FileHelper::createLink(PATH_UPLOAD_AVATAR, $modelFaceBookPage->page_id . '.jpg');
                    if(FileHelper::saveFile($path, $contentFile) === false) {
                        $modelFaceBookPage->avatar = ASSETS_IMAGE_NOT_FOUND;
                    }else{
                        $modelFaceBookPage->avatar = $path;
                    }
                }else{
                    $modelFaceBookPage->avatar = ASSETS_IMAGE_NOT_FOUND;
                }

                $modelFaceBookPage->save();
            }

            if(!empty($paging))
            {
                $objectPaging = new Paging($paging);
                if(!empty($objectPaging->cursors->after))
                {
                    $objectFacebook = FacebookApi::prepare();
                    $newResponse = $objectFacebook->next($response->getGraphEdge());

                    if($newResponse !== null)
                    {
                        return self::savePage($userSetting, $newResponse);
                    }
                }
            }
        }
    }
}
