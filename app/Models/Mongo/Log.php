<?php
namespace App\Models\Mongo;

use App\Components\ModelMongo;

/***
 * Class Log
 * @package App\Models\Mongo
 * @property string $_id
 * @property string $type
 * @property string $error
 * @property string $data
 * @property string $message
 */
class Log extends ModelMongo
{
    const TYPE_YOUTOBE_DOWNLOAD = 'youtobe download';
    const TYPE_FACEBOOK_DOWNLOAD = 'facebook download';

    protected $collection = 'logs';

    protected $primaryKey = '_id';
}
